const menuBlock = document.getElementById('row-menu');
const menuBlockBottom = menuBlock.getBoundingClientRect().bottom + window.pageYOffset;
const logo = document.getElementById('brand-logo');
const menuWrapper= document.getElementById('menu-wrapper');
const menuItems = document.getElementsByClassName('menu-item');
const menuButtons = document.getElementsByClassName('menu-button');
const menuHamburger = document.getElementsByClassName('menubuttondiv');

// const scrollTop = (document.body.scrollTop) ? document.body.scrollTop : document.documentElement.scrollTop;



const columnMenuButton = document.getElementsByClassName('menubutton')[0];
const columnMenu = document.getElementById('column-menu');


columnMenuButton.addEventListener('click', () => {
	if (columnMenu.style.right !== '0px') {
		// columnMenu.style.visibility = 'visible';
		columnMenu.style.right = '0';
	} else {
		// columnMenu.style.visibility = 'hidden';
		columnMenu.style.right = '-50vw';
	}
});

function returnFalse(e){
	e = e||event;
	e.preventDefault ? e.preventDefault() : (e.returnValue = false);
}

$(document).ready(function() {
	
	
	$('.page-about').css('height', $('.page-about-content-wrapper').css('height'))
	
	const toggler = toggle();
	const a = $(".js-calculus");
	a[0].addEventListener("click", () => {
		if (toggler()) {
			$(".calculus").css("visibility", 'visible');
		} else {
			$(".calculus").css("visibility", 'hidden');
		}
	});
	
	document.addEventListener("scroll", returnFalse);
	$(window).scroll(function () {

		if ($(".page-brand-wrapper").css("background-position-y") !== (Math.round($(this).scrollTop() / 4 + 10)) + "px") {
			$(".page-brand-wrapper").css("background-position-y", (Math.round($(this).scrollTop() / 4 + 10)) + "px");
		}

		columnMenu.style.right = '-50vw';
		if (menuBlock.classList.contains('fixed') && $(this).scrollTop() < menuBlockBottom) {
			menuBlock.classList.remove('fixed');
			logo.classList.remove('brand-logo-black');
			logo.classList.add('brand-logo-white');
			menuWrapper.classList.remove('menu-wrapper-scroll');
			[].forEach.call(menuItems, function (el) {
				el.classList.remove('menu-item-scroll');
			});
			[].forEach.call(menuButtons, function (el) {
				el.classList.remove('menu-button-scroll');
			});
			[].forEach.call(menuHamburger, function (el) {
				el.classList.remove('menubuttondiv-black');
			});
		} else if (window.pageYOffset > menuBlockBottom) {
			menuBlock.classList.add('fixed');
			logo.classList.add('brand-logo-black');
			logo.classList.remove('brand-logo-white');
			menuWrapper.classList.add('menu-wrapper-scroll');
			[].forEach.call(menuItems, function (el) {
				el.classList.add('menu-item-scroll');
			});
			[].forEach.call(menuButtons, function (el) {
				el.classList.add('menu-button-scroll');
			});
			[].forEach.call(menuHamburger, function (el) {
				el.classList.add('menubuttondiv-black');
			});
		}
	});
});


function toggle() {
	let isToggled = false;
	return function() {
		isToggled = !isToggled
		return isToggled;
	};
}


